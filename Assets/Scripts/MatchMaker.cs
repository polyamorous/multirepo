﻿using UnityEngine;
using Photon;  
using System.Collections;

public class MatchMaker : Photon.PunBehaviour {
    // Use this for initialization
    public Transform pos1, pos2;
    GameObject player;
    

    void Start()
    {
        DontDestroyOnLoad(this);
        PhotonNetwork.ConnectUsingSettings("0.1");
    }
    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }
    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("Can't join random room!");
        PhotonNetwork.CreateRoom(null);
    }
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.countOfPlayers == 1)
            player = PhotonNetwork.Instantiate("Hero", pos1.position, Quaternion.Euler(new Vector3(0, 0, 0)), 0);
        else
            player = PhotonNetwork.Instantiate("Hero", pos2.position, Quaternion.Euler(new Vector3(0, 0, 0)), 0);
        player.GetComponent<PlayerController>().sStartingPosName = "pos" + PhotonNetwork.countOfPlayers.ToString();
        base.OnJoinedRoom();
        Debug.Log("No i co teraz?");
    }
    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
        if(player != null)
        {
            GUILayout.Label(player.name + " / " + player.transform.position);
        }
    }
}
