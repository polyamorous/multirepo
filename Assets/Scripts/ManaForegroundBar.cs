﻿using UnityEngine;
using System.Collections;

public class ManaForegroundBar : MonoBehaviour {
    public BattleController bcController;
    public float fCurrentMana;
    public float fScale;
    public float fPosX;
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (bcController == null)
        {
            if (GameObject.Find("LocalPlayer"))
                bcController = GameObject.Find("LocalPlayer").GetComponent<BattleController>();
        }
        else
        {
            fCurrentMana = bcController.fCurrentMana;
            float fScale = (fCurrentMana / 10) * 200f;
            float posX = (200 - fScale) * -0.04f;
            transform.localScale = new Vector3(fScale, transform.localScale.y, transform.localScale.z);
            transform.localPosition = new Vector3(posX, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
