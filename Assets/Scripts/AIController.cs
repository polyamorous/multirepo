﻿using UnityEngine;
using System.Collections;

public class AIController : BattleController {
    public Skill[] skillPool;
    public Skill[] chosenSkills = new Skill[3];
    public float speed;
	// Use this for initialization
	void Start () {
        CreateStats();
        ChooseSkills();
        CalculateStats();
        StartCoroutine(ApplySkill());
	}
    IEnumerator ApplySkill()
    {
        while (true)
        {
            int i = Random.Range(0, chosenSkills.Length);
            chosenSkills[i].ChannelSkill();
            yield return new WaitForSeconds(1);
        }
    }
    void ChooseSkills()
    {
        for(int i = 0; i < chosenSkills.Length; i++)
        {
            int j = Random.Range(0, skillPool.Length);
            while (skillPool[j] == null)
            {
                j = Random.Range(0, skillPool.Length);
            }
            chosenSkills[i] = skillPool[j];
            chosenSkills[i].CreateSkill();
            skillPool[j] = null;
        }
    }
	
    void CreateStats()
    {
        stats.iStrength = Random.Range(5, 8);
        stats.iDexterity = Random.Range(5, 8);
        stats.iEndurance = Random.Range(5, 8);
        stats.iIntelligence = Random.Range(5, 8);
    }
    // Update is called once per frame
    // Update is called once per frame
    void Update()
    {
        fCurrentMana = Mathf.MoveTowards(fCurrentMana, iMaxMana, Time.deltaTime * stats.fManaReg);
        if (goEnemy != null)
            transform.position = Vector3.MoveTowards(transform.position, goEnemy.transform.position +  (bcEnemy.bLeft ? Vector3.right : -Vector3.right) * 3f, speed);
        if (fCurrentHP <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void OnMouseDown()
    {
        bcEnemy.goEnemy = gameObject;
        bcEnemy.target = null;
        bcEnemy.bMoving = true;
        bcEnemy.bLeft = true;
        bcEnemy.bRight = false;
    }
}
