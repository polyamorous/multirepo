﻿using UnityEngine;
using System.Collections;
using Photon;

[System.Serializable]
public struct structStats
{
    public int iHP;
    public int iDMG;
    public float fAttSpd;
    public float fManaReg;

    public int iStrength;
    private float fStrengthXP;
    public int iDexterity;
    private float fDexterityXP;
    public int iEndurance;
    private float fEnduranceXP;
    public int iIntelligence;
    private float fIntelligenceXP;
}

public class BattleController : Photon.MonoBehaviour {
    public structStats stats = new structStats();
    public int iMaxMana = 10;
    public float fCurrentMana = 0f;
    public float fCurrentHP;
    public bool bPlayer = false;
    public GameObject goEnemy;
    public Transform floaterParent;
    public BattleController bcEnemy;
    public GameObject floater, floatingText;
    public HealthBar hpBar;
    public ChannelingSkill channelingSkill;
    public bool bInvulnerability = false;
    public bool bGameStart = false;
    public bool bChannelingSkill = false;
    public int iDistanceToEnemy = 0;
    public bool bLeft = true;
    public bool bRight = false;
    public bool bMoving = false;
    public Transform target;

    void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log(player.name);
    }
    


    public void SetRandomStats()
    {
        stats.iStrength = Random.Range(4, 8);
        stats.iDexterity = Random.Range(4, 8);
        stats.iEndurance = Random.Range(4, 8);
        stats.iIntelligence = Random.Range(4, 8);
    }
    public void CalculateStats()
    {
        Debug.Log(stats.iEndurance);
        stats.iHP = stats.iEndurance + stats.iStrength / 2;
        stats.iDMG = stats.iStrength + stats.iIntelligence / 2;
        stats.fAttSpd = 5 / (float)stats.iDexterity - 1 / (float)stats.iEndurance;
        stats.fManaReg = stats.iIntelligence + stats.iDexterity / 2;
        stats.fManaReg /= 5;
        stats.iHP *= 50;
        if (!bGameStart)
        {
            fCurrentHP = stats.iHP;
            StartCoroutine(Attack());
            bGameStart = true;
        }
    }
    IEnumerator Attack()
    {
        while (true)
        {
            if(goEnemy != null)
                iDistanceToEnemy = (int)(transform.position - goEnemy.transform.position).magnitude;
            if (iDistanceToEnemy == 3 && !bMoving)
            {
                if (goEnemy != null && !bcEnemy.bInvulnerability)
                {
                    //bcEnemy.ChangeHP(bcEnemy, -stats.iDMG);
                    bcEnemy.photonView.RPC("ChangeHP2", PhotonTargets.All, -stats.iDMG);
                }
                else
                {
                    if (bcEnemy.bInvulnerability)
                    {
                        floater = PhotonNetwork.Instantiate("FloatingText3D", transform.position, Quaternion.Euler(new Vector3(0, 0, 0)),0) as GameObject;
                        floater.GetComponent<FloatingText>().StartFloatText(bcEnemy.transform, "Invulnerable", .75f, Color.white);
                        floater.transform.parent = floaterParent;
                    }
                }
            }
            yield return new WaitForSeconds(stats.fAttSpd);

        }
    }
    [PunRPC]
    public void ChangeHP(BattleController bcTarget, int iAmount, Color? color = null)
    {
        bcTarget.fCurrentHP += iAmount;
        bcTarget.hpBar.UpdateHP();
        floater = PhotonNetwork.Instantiate("FloatingText3D", transform.position, Quaternion.Euler(new Vector3(0, 0, 0)),0) as GameObject;
        if (color == null)
        {
            floater.GetComponent<FloatingText>().StartFloat(bcTarget.transform, iAmount, 0f, .75f, Color.red);
        }
        else
        {
            floater.GetComponent<FloatingText>().StartFloat(bcTarget.transform, iAmount, 0f, .75f, color);
        }
        floater.transform.parent = floaterParent;
    }
    [PunRPC]
    public void ChangeHP2(int iAmount)
    {
        int i = 0;
        fCurrentHP += iAmount;
        hpBar.UpdateHP();
        CreateFloater(iAmount);
        /**/    
    }
    void CreateFloater(int iAmount)
    {
        Color color = Color.red;
        if(iAmount == 0)
        {
            return;
        }
        floater = GameObject.Instantiate(floatingText, transform.position, Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
        if (color == null)
        {
            floater.GetComponent<FloatingText>().StartFloat(transform, iAmount, 0f, .75f, Color.red);
        }
        else
        {
            floater.GetComponent<FloatingText>().StartFloat(transform, iAmount, 0f, .75f, color);
        }
        floater.transform.parent = floaterParent;
    }
}
