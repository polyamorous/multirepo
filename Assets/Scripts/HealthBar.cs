﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class HealthBar : MonoBehaviour {
    public BattleController bcController;
    public Transform foregroundTransform;
    public Camera uiCam;
    // Use this for initialization
    void Start() {
        StartCoroutine(UpdateHPCoroutine());
        StartCoroutine(UpdatePos());
    }
    void Awake()
    {
        uiCam = GameObject.Find("UICamera").GetComponent<Camera>();
    }
    IEnumerator UpdateHPCoroutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.5f);
            UpdateHP();
            yield return new WaitForSeconds(0.5f);
        }
    }
    public void UpdateHP()
    {
        if(bcController == null)
        {
            return;
        }
        float fCurrentHP = bcController.fCurrentHP;
        int iMaxHP = bcController.stats.iHP;
        float fScale = (fCurrentHP / iMaxHP) * 100f;
        float posX = (100 - fScale) * -0.04f;
        if (fCurrentHP > 0 && iMaxHP != 0)
        { 
            foregroundTransform.localScale = new Vector3(fScale, foregroundTransform.localScale.y, foregroundTransform.localScale.z);
            foregroundTransform.localPosition = new Vector3(posX, foregroundTransform.localPosition.y, foregroundTransform.localPosition.z);
        }
        else
        {
            //foregroundTransform.localScale = new Vector3(0, 0, 0);
        }
    }

    IEnumerator UpdatePos()
    {
        while (true)
        {
            if(bcController != null)
                transform.position = uiCam.ScreenToWorldPoint(Camera.main.WorldToScreenPoint(bcController.transform.position)) + Vector3.up;
            yield return new WaitForEndOfFrame();
        }
    }
	// Update is called once per frame
	void Update () {
	
	}
}
