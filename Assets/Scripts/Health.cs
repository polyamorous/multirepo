﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour
{
    public int iHealth;
    public int iMaxHealth;
    public BattleController player;

    void Awake()
    {
        player = GetComponent<BattleController>();
        CmdSetMaxHealth(player.stats.iHP);
        CmdSetHealth(iMaxHealth);
    }

    void CmdSetMaxHealth(int newHealth)
    {
        iMaxHealth = newHealth;
    }

    void CmdSetHealth(int newHealth)
    {
        Debug.Log("Taking DMG" + name + newHealth);
        iHealth = newHealth;
    }

    void OnHealthChanged(int newHealth)
    {
        iHealth = newHealth;
        player.hpBar.UpdateHP();
        player.floater = PhotonNetwork.Instantiate("FloatingText3D", player.transform.position, Quaternion.Euler(new Vector3(0, 0, 0)),0) as GameObject;
        player.floater.GetComponent<FloatingText>().StartFloat(player.transform, newHealth, 0f, .75f, Color.red);

        player.floater.transform.parent = player.floaterParent;
    
        if (iHealth < 100)
        {
     //       StartCoroutine(ShowHitEffect());
        }
    }
    public void TakeDamage(int iAmount)
    {
        Debug.Log("?" + iAmount);
        iHealth += iAmount;
        //player.hpBar.UpdateHP();
        //player.floater = GameObject.Instantiate(player.floatingText, player.transform.position, Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
        //player.floater.GetComponent<FloatingText>().StartFloat(player.transform, iAmount, 0f, .75f, Color.red);

        //player.floater.transform.parent = player.floaterParent;
        //RpcChangeHP(iAmount);
    }
}