﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum SkillTarget { Player, Enemy, Both };

[System.Serializable]
public enum StatChange { STR, DEX, INT, END };

public class OptionalOut<Type>
{
    public Type Result { get; set; }
}

[System.Serializable]
public struct SkillType
{
    public SkillTarget skillTarget;

    public string sHPChangeFormula;
    public int iHPChange;
    public bool bHPChangeOverTime;
    public string sHPCOTAmountFormula;
    public int iHPCOTAmount;
    public string sHPCOTIntervalFormula;
    public float fHPCOTInterval;
    public string sHPCOTTotalFormula;
    public float fHPCOTTotal;

    public StatChange statChange;
    public string sStatChangeFormula;
    public int iStatChangeAmount;

    public string sInvulnerabilityTimerFormula;
    public float fInvulnerabilityTimer;
}
public class Skill : MonoBehaviour {

    public int iManaCost = 1;
    public int iRechargeTime = 1;
    public int iChannelingTime = 1;

    public SpriteRenderer sprite;

    public BattleController bcController;
    public BattleController bcTargetController = null;
    public BattleController bctargetController2 = null;

    public bool bActivated = false;
    public bool bMouseOver = false;
    public bool bRefreshing = true;

    public string sName, sDesc;
    public Transform tTooltip;
    public Transform tRefreshMask;
    public TextMesh tmName, tmDesc;

    public SkillType skillType;
    public int iNumberOfIntervals = 0;

	// Use this for initialization
	void Start () {
        sprite = GetComponent<SpriteRenderer>();
        StartCoroutine(CheckAvailability());
//        CreateSkill();
	}
	
    public void CreateSkill()
    {

        switch (skillType.skillTarget)
        {
            case SkillTarget.Player:
                bcTargetController = bcController;
                break;
            case SkillTarget.Enemy:
                bcTargetController = bcController.bcEnemy;
                break;
            case SkillTarget.Both:
                bcTargetController = bcController;
                bctargetController2 = bcController.bcEnemy;
                break;
        }
        skillType.iHPChange = StringToIntFormula(skillType.sHPChangeFormula);
        skillType.iHPCOTAmount = StringToIntFormula(skillType.sHPCOTAmountFormula);
        skillType.fHPCOTInterval = StringToFloatFormula(skillType.sHPCOTIntervalFormula);
        skillType.fHPCOTTotal = StringToFloatFormula(skillType.sHPCOTTotalFormula);

        skillType.iStatChangeAmount = StringToIntFormula(skillType.sStatChangeFormula);
        skillType.fInvulnerabilityTimer = StringToFloatFormula(skillType.sInvulnerabilityTimerFormula);

        sDesc = sDesc.Replace("[HPChange]", skillType.iHPChange.ToString());
        sDesc = sDesc.Replace("[HPCOTAmount]", skillType.iHPCOTAmount.ToString());
        sDesc = sDesc.Replace("[HPCOTInterval]", skillType.fHPCOTInterval.ToString());
        sDesc = sDesc.Replace("[HPCOTTotal]", skillType.fHPCOTTotal.ToString());
        sDesc = sDesc.Replace("[StatChange]", skillType.iStatChangeAmount.ToString());
        sDesc = sDesc.Replace("[InvulnerabilityTimer]", skillType.fInvulnerabilityTimer.ToString());
    }

    int StringToIntFormula(string sFormula)
    {
        int iFormula = 0;
        if (sFormula.Length > 0)
        { 
            switch (sFormula.Substring(0, 5))
            {
                case "[STR]":
                    iFormula = bcController.stats.iStrength;
                    Debug.Log(bcController.stats.iStrength);
                    break;
                case "[INT]":
                    iFormula = bcController.stats.iIntelligence;
                    break;
                case "[DEX]":
                    iFormula = bcController.stats.iDexterity;
                    break;
                case "[END]":
                    iFormula = bcController.stats.iEndurance;
                    break;
                default:
                    break;
            }
            if (sFormula.Length > 5)
            {
                switch (sFormula.Substring(5, 1))
                {
                    case "+":
                        iFormula += int.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;
                    case "*":
                        iFormula *= int.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;
                    case "/":
                        iFormula /= int.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;
                    case "-":
                        iFormula -= int.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;

                }
            }
        }
        return iFormula;
    }

    float StringToFloatFormula(string sFormula)
    {
        float fFormula = 0;
        if (sFormula.Length > 0)
        {
            switch (sFormula.Substring(0, 5))
            {
                case "[STR]":
                    fFormula = bcController.stats.iStrength;
                    break;
                case "[INT]":
                    fFormula = bcController.stats.iIntelligence;
                    break;
                case "[DEX]":
                    fFormula = bcController.stats.iDexterity;
                    break;
                case "[END]":
                    fFormula = bcController.stats.iEndurance;
                    break;
            }
            if (sFormula.Length > 5)
            {
                switch (sFormula.Substring(5, 1))
                {
                    case "+":
                        fFormula += float.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;
                    case "*":
                        fFormula *= float.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;
                    case "/":
                        fFormula /= float.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;
                    case "-":
                        fFormula -= float.Parse(sFormula.Substring(6, sFormula.Length - 6));
                        break;

                }
            }
        }
        return fFormula;
    }

    IEnumerator CheckAvailability()
    {
        while (true)
        {
            if (bcController == null)
            {
                if (GameObject.Find("LocalPlayer"))
                    bcController = GameObject.Find("LocalPlayer").GetComponent<BattleController>();
            }
            else
            {
                if (!bRefreshing)
                {

                    if (bcController.fCurrentMana > iManaCost - 0.1f)
                    {
                        sprite.color = Color.white;
                        bActivated = true;
                    }
                    else
                    {
                        sprite.color = Color.gray;
                        bActivated = false;
                    }
                }
                else
                {
                    sprite.color = Color.gray;
                    bActivated = false;
                }
            }
            yield return new WaitForSeconds(1f);
        }
    }

    void OnMouseDown()
    {
        CreateSkill();
        tTooltip.gameObject.SetActive(true);
        sDesc = sDesc.Replace("\\n", "\n");
        tmName.text = sName;
        tmDesc.text = sDesc;
        tTooltip.position = transform.position + Vector3.up * 3f + Vector3.forward * 3f;
    }
    void OnMouseOver()
    {
        if(!bMouseOver)
            bMouseOver = true;
    }
    void OnMouseExit()
    {
        if (bMouseOver)
            bMouseOver = false;
       // tTooltip.gameObject.SetActive(false);
    }       
    void OnMouseUp()
    {
        if (bMouseOver)
        {
            ChannelSkill();
        }
        tTooltip.gameObject.SetActive(false);
    }
    public void ChannelSkill()
    {
        CheckAvailability();
        if (bActivated && !bcController.bChannelingSkill)
        {
            bcController.fCurrentMana -= iManaCost;
            tRefreshMask.localScale = new Vector3(1, 1, 1);
            tRefreshMask.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            bRefreshing = true;
            bActivated = false;
            ChannelingSkill channelingSkill = bcController.channelingSkill;
            channelingSkill.fChargingTime = iChannelingTime;
            channelingSkill.GetComponent<SpriteRenderer>().sprite = sprite.sprite;
            channelingSkill.sName = sName;
            channelingSkill.sDesc = sDesc;
            channelingSkill.skill = this;
            channelingSkill.bcController = bcController;
            bcController.bChannelingSkill = true;
            channelingSkill.gameObject.SetActive(true);
        }
    }
    public void ApplySkill()
    {
        if (skillType.iHPChange < 0)
        {
            //bcTargetController.ChangeHP(bcTargetController, skillType.iHPChange);
            bcTargetController.photonView.RPC("ChangeHP2", PhotonTargets.All, skillType.iHPChange);
        }
        else if (skillType.iHPChange == 0)
        {

        }
        else
        {
            //bcTargetController.ChangeHP(bcTargetController, skillType.iHPChange, Color.green);
            bcTargetController.photonView.RPC("ChangeHP2", PhotonTargets.All, skillType.iHPChange);
        }

        if (skillType.bHPChangeOverTime)
        {
            iNumberOfIntervals = (int)(skillType.fHPCOTTotal / skillType.fHPCOTInterval);
            StartCoroutine("ChangeHP");
        }

        if (skillType.iStatChangeAmount != 0)
        {
            switch (skillType.statChange)
            {
                case StatChange.DEX:
                    bcTargetController.stats.iDexterity += skillType.iStatChangeAmount;
                    break;
                case StatChange.INT:
                    bcTargetController.stats.iIntelligence += skillType.iStatChangeAmount;
                    break;
                case StatChange.END:
                    bcTargetController.stats.iEndurance += skillType.iStatChangeAmount;
                    break;
                case StatChange.STR:
                    bcTargetController.stats.iStrength += skillType.iStatChangeAmount;
                    break;
            }
            bcTargetController.stats.iDexterity = Mathf.Max(1, bcTargetController.stats.iDexterity);
            bcTargetController.stats.iIntelligence = Mathf.Max(1, bcTargetController.stats.iIntelligence);
            bcTargetController.stats.iEndurance = Mathf.Max(1, bcTargetController.stats.iEndurance);
            bcTargetController.stats.iStrength = Mathf.Max(1, bcTargetController.stats.iStrength);

            bcTargetController.CalculateStats();
        }

        if (skillType.fInvulnerabilityTimer > 0)
        {
            bcTargetController.bInvulnerability = true;
            StartCoroutine(CancelInvulnerability());
        }


        transform.parent.gameObject.BroadcastMessage("CheckAvailability");
        //    CheckAvailability();
    }


    IEnumerator CancelInvulnerability()
    {
        yield return new WaitForSeconds(skillType.fInvulnerabilityTimer);
        bcTargetController.bInvulnerability = false;
    }
    IEnumerator ChangeHP()
    {
        while (iNumberOfIntervals > 0)
        {
            if (iNumberOfIntervals > 0)
            {
                if (skillType.iHPCOTAmount < 0)
                {
                    //bcTargetController.ChangeHP(bcTargetController, skillType.iHPCOTAmount);
                    bcTargetController.photonView.RPC("ChangeHP2", PhotonTargets.All, skillType.iHPCOTAmount);
                }
                else if (skillType.iHPCOTAmount == 0)
                {

                }
                else
                {
                    //bcTargetController.ChangeHP(bcTargetController, skillType.iHPCOTAmount, Color.green);
                    bcTargetController.photonView.RPC("ChangeHP2", PhotonTargets.All, skillType.iHPCOTAmount);
                }
            }
            iNumberOfIntervals--;
            yield return new WaitForSeconds(skillType.fHPCOTInterval);
        }
    }

	// Update is called once per frame
	void Update () {
        if(bRefreshing)
        {
            Vector3 v3FinalScale = new Vector3(16, 18, 1);
            Vector3 v3FinalRotation = new Vector3(0, 0, 180);
            tRefreshMask.gameObject.SetActive(true);
            
            tRefreshMask.localScale = Vector3.MoveTowards(tRefreshMask.localScale, v3FinalScale, (Time.deltaTime*18f)/iRechargeTime);
            tRefreshMask.rotation = Quaternion.Euler(Vector3.MoveTowards(tRefreshMask.rotation.eulerAngles, v3FinalRotation, (Time.deltaTime*150)/iRechargeTime));
            if(tRefreshMask.localScale == v3FinalScale)
            {
                StartCoroutine(Broadcast());
                tRefreshMask.gameObject.SetActive(false);
                bRefreshing = false;
                CheckAvailability();
            }
        }
	}
    
    IEnumerator Broadcast()
    {
        yield return new WaitForSeconds(0.1f);
        transform.parent.gameObject.BroadcastMessage("CheckAvailability");
    }
}
