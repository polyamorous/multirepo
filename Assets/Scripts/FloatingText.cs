﻿using UnityEngine;
using System.Collections;

public class FloatingText : MonoBehaviour
{
    public Transform target;
    public Vector3 lastTargetPos;
    public Vector3 targetUp;
    public float fStrength;
    public string sText;
    public TextMesh textMesh;
    public void StartFloat(Transform hitObject, float fDamage, float fDefense = 0f, float fSize = 1f, Color? color = null)
    {
        
        target = hitObject;
        fStrength = fDamage;
        StartCoroutine(Float());
        textMesh.color = color ?? Color.black;
        textMesh.characterSize *= fSize;
    }
    public void StartFloatText(Transform hitObject, string text, float fSize = 1f, Color? color = null)
    {
        target = hitObject;
        sText = text;
        StartCoroutine(FloatText());
        textMesh.color = color ?? Color.black;
        textMesh.characterSize *= fSize;
    }
    IEnumerator FloatText()
    {
        float i = 0.0f;
        float rate = 1.0f / 1.5f;
        if (target != null)
        {
            textMesh.text = sText;
        }
        while (i < 0.5f)
        {
            i += Time.deltaTime * rate;
            if (target != null)
            {
                lastTargetPos = target.position;
                targetUp = target.up;
            }
            gameObject.transform.position = Vector3.Lerp(lastTargetPos, lastTargetPos + targetUp * 10f, i);
            yield return null;
        }
        Destroy(gameObject);
    }
    IEnumerator Float()
    {
        float i = 0.0f;
        float rate = 1.0f / 1.5f;
        if (target != null)
        {
            if (target.name == "PlayerCharacter" || (target.parent != null && target.parent.name == "PlayerCharacter"))
            {
                textMesh.color = Color.red;
            }
            else
            {
                textMesh.color = Color.black;
            }
            textMesh.text = ((int)fStrength).ToString();
        }
        while (i < 0.5f)
        {
            i += Time.deltaTime * rate;
            if (target != null)
            {
                lastTargetPos = target.position;
                targetUp = target.up;
            }
            gameObject.transform.position = Vector3.Lerp(lastTargetPos, lastTargetPos + targetUp * 10f, i);
            yield return null;
        }
        Destroy(gameObject);
    }
}
