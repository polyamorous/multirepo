﻿using UnityEngine;
using Photon;
using System.Collections;

public class ChannelingSkill : Photon.MonoBehaviour {

    public float fChargingTime;
    public bool bMouseOver = true;
    public bool bChanneling = true;

    public string sName, sDesc;
    public Transform tTooltip;
    public Transform tRefreshMask;
    public TextMesh tmName, tmDesc;
    public Skill skill;
    public BattleController bcController;
    public Camera uiCam;
    void Awake()
    {
        //tRefreshMask = GameObject.Find("RefreshMask").transform;
        uiCam = GameObject.Find("UICamera").GetComponent<Camera>();
    }
    void OnMouseDown()
    {
        if(tTooltip == null)
        {
            tTooltip = GameObject.Find("ChannelingSkillTooltip").transform;
        }
        tTooltip.gameObject.SetActive(true);
        sDesc = sDesc.Replace("\\n", "\n");
        tmName.text = sName;
        tmDesc.text = sDesc;
        tTooltip.position = uiCam.ScreenToWorldPoint(Camera.main.WorldToScreenPoint(bcController.transform.position)) + Vector3.down * 2f;
    }
    void OnMouseOver()
    {
        if (!bMouseOver)
            bMouseOver = true;
    }
    void OnMouseExit()
    {
        if (bMouseOver)
            bMouseOver = false;
        // tTooltip.gameObject.SetActive(false);
    }
    void OnMouseUp()
    {
        tTooltip.gameObject.SetActive(false);
    }

    public void OnEnable()
    {
        if(tRefreshMask == null)
        {
            tRefreshMask = GameObject.Find("RefreshMask").transform;
        }
        tRefreshMask.localScale = new Vector3(1, 1, 1);
        tRefreshMask.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }
    void Update()
    {
        if (bChanneling)
        {
            transform.position = uiCam.ScreenToWorldPoint(Camera.main.WorldToScreenPoint(bcController.transform.position)) + Vector3.up * 2f;
            Vector3 v3FinalScale = new Vector3(16, 18, 1);
            Vector3 v3FinalRotation = new Vector3(0, 0, 180);

            tRefreshMask.gameObject.SetActive(true);
            
            tRefreshMask.localScale = Vector3.MoveTowards(tRefreshMask.localScale, v3FinalScale, (Time.deltaTime * 18f) / fChargingTime);
            tRefreshMask.rotation = Quaternion.Euler(Vector3.MoveTowards(tRefreshMask.rotation.eulerAngles, v3FinalRotation, (Time.deltaTime * 150) / fChargingTime));
            if (tRefreshMask.localScale == v3FinalScale)
            {
                skill.ApplySkill();
                bcController.bChannelingSkill = false;
                tRefreshMask.gameObject.SetActive(false);
                if(tTooltip.gameObject.activeSelf)
                {
                    tTooltip.gameObject.SetActive(false);
                }
                gameObject.SetActive(false);
                //bChanneling = false;
            }
        }
    }
}
