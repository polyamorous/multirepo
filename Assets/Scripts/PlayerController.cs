﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerController : BattleController {
    public Transform center;
    public float speed;
    public Vector3 distanceToCenter;
    public string sStartingPosName;
    void Awake()
    {
        center = GameObject.Find("center").transform;
        SetRandomStats();
        CalculateStats();
        if(photonView.isMine)
        {
            name = "LocalPlayer";
        }
        else
        {
            name = "ENEMY";
        }
    }
    // Update is called once per frame
    void Update()
    {
        fCurrentMana = Mathf.MoveTowards(fCurrentMana, iMaxMana, Time.deltaTime * stats.fManaReg);
        if (SceneManager.GetActiveScene().name == "test")
        {
            if (goEnemy != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, goEnemy.transform.position - Vector3.right * 3f, speed);
            }
        }
        else
        {
            
            distanceToCenter = transform.position - center.position;
            if (goEnemy != null)
            {
                if (sStartingPosName == "pos1")
                {
                    transform.position = Vector3.MoveTowards(transform.position, center.position - Vector3.right * 1.5f, speed);
                    if (transform.position == center.position - Vector3.right * 1.5f)
                    {
                        bMoving = false;
                    }
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, center.position + Vector3.right * 1.5f, speed);
                    if (transform.position == center.position + Vector3.right * 1.5f)
                    {
                        bMoving = false;
                    }
                }
            }
            if (target != null && target != goEnemy)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.position - distanceToCenter * 0.5f, speed * 1.05f);
                //Debug.Log(transform.position + " / " + (target.position - distanceToCenter * 0.5f) + " / " + speed);
                if(transform.position == target.position - distanceToCenter * 0.5f)
                {
                    bMoving = false;
                }
            }
        }
        if (fCurrentHP <= 0)
        {
            Destroy(this.gameObject);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }
    
    void OnMouseDown()
    {
        PlayerController attacker;
        if(name == "LocalPlayer")
        {
            Debug.Log("PLAYER");
        }
        else
        {
            Debug.Log("ENEMY");
            attacker = GameObject.Find("LocalPlayer").GetComponent<PlayerController>();
            attacker.bcEnemy = this;
            attacker.goEnemy = this.gameObject;
            attacker.target = null;
            attacker.bMoving = true;
        }

    }
}
